using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using Microsoft.Win32;
using NotifySend;
using Ookii.CommandLine;

namespace SetWallpaper;

public static class WallpaperSetter {
    public static void Set(string[] args) {
#if !DEBUG
        var listeners = new TraceListener[] { new TextWriterTraceListener(Console.Out) };
        Trace.Listeners.AddRange(listeners);
#endif
        
        var parser = new CommandLineParser<Options>();
        
        Options arguments;
        try {
            arguments = parser.Parse(args) ?? throw new InvalidOperationException("Parsed options are null");
        }
        catch (CommandLineArgumentException ex) {
            Console.WriteLine(ex.Message);
            parser.WriteUsage();
            Environment.Exit(-1);
            return;
        }
        
        if (string.IsNullOrEmpty(arguments.File)) {
            Console.WriteLine("No se especificó un archivo.");
            Environment.Exit(-1);
            return;
        }
        
        var fullPath = Path.GetFullPath(arguments.File);
        
        if (!File.Exists(fullPath)) {
            Console.WriteLine($"El archivo {fullPath} no existe");
            Environment.Exit(-1);
            return;
        }
        
        Console.WriteLine($"Setting {fullPath} as wallpaper");
        
        var strategy = WallpaperStrategy.Calculate(fullPath, arguments.BgColorPercentile);
        
        Trace.WriteLine(strategy);
        
        //This is how we set the desktop background (Using the Registry and the API)
        var key = Registry.CurrentUser.OpenSubKey(@"Control Panel\Desktop", true);
        if (key != null) {
            key.SetValue(@"PicturePosition", strategy.Style.ToStringInt());
            key.SetValue(@"TileWallpaper", strategy.Tiled ? "1" : "0");
            key.SetValue(@"WallpaperStyle", strategy.Style.ToStringInt());
            key.SetValue(@"WallPaper", fullPath.ToLower());
            key.Close();
        }
        
        var colorsKey = Registry.CurrentUser.OpenSubKey(@"Control Panel\Colors", true);
        if (colorsKey != null) {
            colorsKey.SetValue(@"Background", strategy.BackgroundColor.ToRegistryString());
            colorsKey.Close();
        }
        
        int[] elements = [
            1 //COLOR_DESKTOP
        ];
        
        Externs.SetSysColors(elements.Length, elements, new[] { ColorTranslator.ToWin32(strategy.BackgroundColor) });
        
        const int setDesktopBackground = 20;
        const int updateIniFile = 1;
        const int sendWindowsIniChange = 2;
        
        var result =
            Externs.SystemParametersInfo(setDesktopBackground, 0, fullPath, updateIniFile | sendWindowsIniChange);
        if (result == 0) {
            Trace.WriteLine("Error updating SystemParameters");
        }
        
        if (arguments.Notify) {
            SendNotification(fullPath);
        }
    }
    
    private static void SendNotification(string fullPath) {
        var options = new NotificationOptions {
            Title = "Wallroller",
            Body = $"File: {Path.GetFileName(fullPath)}",
            File = fullPath
        };
        
        Notifier.Send(options);
    }
}