﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Drawing;

namespace SetWallpaper;

public enum WallpaperStyle {
    FILL = 10,
    FIT = 6,
    CENTERED = 0
}

[Flags]
public enum FillingBars {
    NONE = 0,
    LEFT_RIGHT = 1,
    TOP_BOTTOM = 2,
    AROUND = LEFT_RIGHT + TOP_BOTTOM
}

public static class FillingBarsHelper {
    public static bool Contains(this FillingBars fillingBars, FillingBars testedValue) {
        return (testedValue & fillingBars) != FillingBars.NONE;
    }
}

internal class WallpaperStrategy(WallpaperStyle style, bool tiled, Color backgroundColor) {
    private const double CenterScreenImageProportionThreshold = 0.60d;
    private const double TileScreenImageProportionThreshold = 0.4d;
    private const double FillProportionRatioThreshold = 0.20d;
    
    public Color BackgroundColor { get; } = backgroundColor;
    public bool Tiled { get; } = tiled;
    public WallpaperStyle Style { get; } = style;
    
    public override string ToString() {
        return $"Color: {BackgroundColor}, Tiled: {Tiled}, Style: {Style}";
    }
    
    public static WallpaperStrategy Calculate(string source, int bgColorPercentile) {
        if (bgColorPercentile < 0) {
            bgColorPercentile = 0;
        }
        
        if (bgColorPercentile > 100) {
            bgColorPercentile = 100;
        }
        
        Color color;
        WallpaperStyle style;
        var tiled = false;
        
        using var bitmap = (Bitmap)Image.FromFile(source);
        double bitmapWidth = bitmap.Width;
        double bitmapHeight = bitmap.Height;
        
        var proportion = bitmapWidth / bitmapHeight;
        
        double screenWidth = Externs.GetSystemMetrics(Externs.SystemMetric.SM_CXSCREEN);
        double screenHeight = Externs.GetSystemMetrics(Externs.SystemMetric.SM_CYSCREEN);
        
        var screenProportion = screenWidth / screenHeight;
        
        var proportionDeltaRaw = screenProportion - proportion;
        var proportionDelta = Math.Abs(proportionDeltaRaw);
        var fillingBars = proportionDeltaRaw > 0 ? FillingBars.LEFT_RIGHT : FillingBars.TOP_BOTTOM;
        
        var proportionRatio = proportionDelta / screenProportion;
        
        if (bitmapWidth < CenterScreenImageProportionThreshold * screenWidth &&
            bitmapHeight < CenterScreenImageProportionThreshold * screenHeight) {
            style = WallpaperStyle.CENTERED;
            if (bitmapWidth < TileScreenImageProportionThreshold * screenWidth) {
                tiled = true;
            }
            
            color = GetAverageColor(bitmap, FillingBars.AROUND, bgColorPercentile);
        }
        else if (proportionRatio <= FillProportionRatioThreshold) {
            style = WallpaperStyle.FILL;
            color = GetAverageColor(bitmap, FillingBars.AROUND, bgColorPercentile);
        }
        else {
            style = WallpaperStyle.FIT;
            color = GetAverageColor(bitmap, fillingBars, bgColorPercentile);
        }
        
        return new WallpaperStrategy(style, tiled, color);
    }
    
    private static Color GetAverageColor(Bitmap bitmap, FillingBars fillingBars, int bgColorPercentile) {
        var rMedianer = new Medianer();
        var gMedianer = new Medianer();
        var bMedianer = new Medianer();
        var alphaWitness = new Witness();
        
        if (fillingBars.Contains(FillingBars.TOP_BOTTOM)) {
            for (var s = 0; s < bitmap.Width; s += 1)
                foreach (var t in new[] { 0 /*, bitmap.Height - 1*/ }) {
                    var pixel = bitmap.GetPixel(s, t);
                    
                    rMedianer.AddValue(pixel.R);
                    gMedianer.AddValue(pixel.G);
                    bMedianer.AddValue(pixel.B);
                    alphaWitness.Observe(pixel.A);
                }
        }
        
        if (fillingBars.Contains(FillingBars.LEFT_RIGHT)) {
            for (var t = 0; t < bitmap.Height; t += 1)
                foreach (var s in new[] {
                             /*0, */bitmap.Width - 1
                         }) {
                    var pixel = bitmap.GetPixel(s, t);
                    
                    rMedianer.AddValue(pixel.R);
                    gMedianer.AddValue(pixel.G);
                    bMedianer.AddValue(pixel.B);
                    alphaWitness.Observe(pixel.A);
                }
        }
        
        if (alphaWitness.HasWitnessed) {
            // Image has trasparency, better to use black
            return Color.Black;
        }
        
        var r = rMedianer.GetMedian(bgColorPercentile);
        var g = gMedianer.GetMedian(bgColorPercentile);
        var b = bMedianer.GetMedian(bgColorPercentile);
        
        Trace.WriteLine($"R: {r}");
        Trace.WriteLine($"G: {g}");
        Trace.WriteLine($"B: {b}");
        
        return Color.FromArgb(0, r, g, b);
    }
}

public static class WallpaperStyleExtensions {
    public static string ToStringInt(this WallpaperStyle style) {
        return ((int)style).ToString();
    }
}

public static class ColorExtension {
    public static string ToRegistryString(this Color color) {
        return $"{color.R} {color.G} {color.B}";
    }
}

internal class Witness {
    public bool HasWitnessed { get; private set; }
    
    public void Observe(int pixelAlpha) {
        if (pixelAlpha != 0xFF) {
            HasWitnessed = true;
        }
    }
}

internal class Medianer {
    private readonly Dictionary<int, int> _rList;
    private long _total;
    
    internal Medianer() {
        _rList = new Dictionary<int, int>();
    }
    
    internal void AddValue(int value) {
        if (!_rList.TryAdd(value, 1)) {
            _rList[value] += 1;
        }
        
        _total += 1;
    }
    
    internal int GetMedian(int percentile) {
        var rSorted = _rList.ToImmutableList().Sort((pair, valuePair) => valuePair.Value - pair.Value);
        
        var counter = _total * percentile / 100;
        
        long totalCount = 0;
        long totalValue = 0;
        
        foreach (var current in rSorted) {
            if (counter <= 0) {
                break;
            }
            
            var factor = counter > current.Value ? current.Value : counter;
            
            totalCount += factor;
            totalValue += current.Key * factor;
            counter -= factor;
        }
        
        if (totalCount == 0) {
            return 0;
        }
        
        return (int)(totalValue / totalCount);
    }
}