﻿using System;

namespace SetWallpaper;

public static class Program {
    [STAThread]
    private static void Main(string[] args) {
        WallpaperSetter.Set(args);
    }
}