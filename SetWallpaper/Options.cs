﻿using Ookii.CommandLine;

namespace SetWallpaper;

internal class Options {
    [CommandLineArgument(Position = 0, ValueDescription = "Path of the image.", IsRequired = true)]
    public string File { get; set; } = string.Empty;
    
    [CommandLineArgument(ValueDescription = "Notify change", DefaultValue = false)]
    public bool Notify { get; set; }
    
    [CommandLineArgument(ValueDescription = "Percentile of edge color components to consider for background",
        DefaultValue = 60)]
    public int BgColorPercentile { get; set; }
}