﻿using SetWallpaper;

namespace SetWallpaperW;

internal class Program {
    private static void Main(string[] args) {
        WallpaperSetter.Set(args);
    }
}