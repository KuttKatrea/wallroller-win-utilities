using System;
using System.IO;
using System.Threading;
using Microsoft.Toolkit.Uwp.Notifications;

namespace NotifySend;

public static class Notifier {
    public static void Send(NotificationOptions arguments) {
        var toastBuilder = new ToastContentBuilder();
        
        if (arguments.Title != null) {
            toastBuilder.AddText(arguments.Title);
        }
        
        if (arguments.Body != null) {
            toastBuilder.AddText(arguments.Body);
        }
        
        var file = arguments.File ?? throw new InvalidOperationException("File not specified");
        var source = arguments.Source ?? file;
        
        toastBuilder.AddInlineImage(new Uri(file))
            .SetProtocolActivation(new Uri(source)).AddButton(
                new ToastButton("Open local file", arguments.File) {
                    ActivationType = ToastActivationType.Protocol
                }).AddButton(
                new ToastButton("Open local folder", Path.GetDirectoryName(file)) {
                    ActivationType = ToastActivationType.Protocol
                }).Show(toast => {
                toast.Tag = "current";
                toast.Group = "wallroller";
            });
        
        
        if (arguments.WaitSeconds > 0) {
            Thread.Sleep(arguments.WaitSeconds * 1000);
        }
    }
}