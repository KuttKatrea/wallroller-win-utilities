﻿using Ookii.CommandLine;

namespace NotifySend;

public record NotificationOptions {
    [CommandLineArgument(Position = 0, ValueDescription = "URL of the image.", IsRequired = true)]
    public string? File { get; init; }
    
    [CommandLineArgument(ValueDescription = "Source URL of the image.")]
    public string? Source { get; init; }
    
    [CommandLineArgument(ValueDescription = "Title of the notification")]
    public string? Title { get; init; }
    
    [CommandLineArgument(ValueDescription = "Body of the notification")]
    public string? Body { get; init; }
    
    [CommandLineArgument(ValueDescription = "Seconds to wait for the notification before exiting", DefaultValue = 0)]
    public int WaitSeconds { get; init; }
}