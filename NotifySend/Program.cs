﻿using System;
using Ookii.CommandLine;

namespace NotifySend;

internal static class Program {
    [STAThread]
    private static void Main(string[] args) {
        var parser = new CommandLineParser<NotificationOptions>();
        NotificationOptions arguments;
        try {
            arguments = parser.Parse(args) ?? throw new InvalidOperationException("Options are null");
        }
        catch (CommandLineArgumentException ex) {
            Console.WriteLine(ex.Message);
            parser.WriteUsage();
            return;
        }
        
        Notifier.Send(arguments);
    }
}