# Wallroller Windows Utilities

Here are two simple utilities for using with my wallroller project:

## SetWallpaper

Utility to set a local image as wallpaper, calculating automatically which layout suites better according to the current size of the screen, and setting a suitable background color.

## NotifySend

Utility to notify when a wallpaper change has occurred.
Both utilities are targeted to Windows 10, so they may not work correctly in an older version of windows.

# Copyright and license

This project is licensed under the terms of the MIT license.

This project may include code blocks from https://github.com/nels-o/toaster, used under the terms of the Creative Commons Attribution 3.0 license


